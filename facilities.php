<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Camping de luxe - Installation</title>
    <!-- CSS -->
	<link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css"/>
	<?php require('inc/links.php'); ?>
</head>
<body class="bg-light">
	<?php require('inc/header.php'); ?>

	<div class="my-5 px-4">
		<h2 class="fw-bold text-center">NOS INSTALLATIONS</h2>
		<div class="h-line bg-dark"></div>
		<p class="text-center mt-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi quam, in exercitationem dignissimos labore voluptates officiis libero architecto molestias molestiae.</p>
	</div>
	<div class="container">
		<div class="row">
			<?php 
				$res = selectAll('facilities');
				$path = FACILITY_IMG_PATH;

				while($row = mysqli_fetch_assoc($res)){
					echo<<<data
					<div class="col-lg-4 col-md-6 mb-5 px-4">
						<div class="bg-white rounded shadow p-4 border-top border-4 border-dark pop">
							<div class="d-flex align-items-center mb-2">
								<img src="$path$row[icon]" width="40px" alt="wifi">
								<h5 class="m-0 ms-3">$row[name]</h5>
							</div>
							<p>$row[description]</p>
						</div>
					</div>
					data;
				}
			?>
		</div>
	</div>

	<?php  require('inc/footer.php'); ?>
<!-- JS -->
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script src="./js/main.js"></script>
</body>
</html>
