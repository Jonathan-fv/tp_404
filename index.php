<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Camping de luxe</title>
    <!-- CSS -->
	<link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css"/>
	<?php require('inc/links.php'); ?>
</head>
<body class="bg-light">
	<?php require('inc/header.php'); ?>

	<!-- Carrousel -->
	<div class="container-fluid px-lg-4 mt-4">
		<div class="swiper swipper-container">
			<div class="swiper-wrapper">
				<?php 
					$res = selectAll('carousel');

					while($row = mysqli_fetch_assoc($res)){
						$path = CAROUSEL_IMG_PATH; 
						echo <<<data
							<div class="swiper-slide">
								<img class="w-100 d-block" src="$path$row[image]" />
							</div>
						data;
					}
				?>
			</div>
		</div>
	</div>

	<!-- Check availability form -->
	<div class="container availability-form">
		<div class="row">
			<div class="col-lg-12 bg-white shadow p-4 rounded">
				<h5 class="mb-4">Vérifier la disponibilité de la réservation</h5>
				<form>
					<div class="row align-items-end">
						<div class="col-lg-3 mb-3">
							<label class="form-label" style="font-weight: 500">Début du séjour</label>
							<input type="date" class="form-control shadow-none">
						</div>
						<div class="col-lg-3 mb-3">
							<label class="form-label" style="font-weight: 500">Fin du séjour</label>
							<input type="date" class="form-control shadow-none">
						</div>
						<div class="col-lg-3 mb-3">
							<label class="form-label" style="font-weight: 500">Adulte(s)</label>
							<select class="form-select shadow-none">
								<option selected>Choisissez un nombre</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</div>
						<div class="col-lg-2 mb-3">
							<label class="form-label" style="font-weight: 500">Enfant(s)</label>
							<select class="form-select shadow-none">
								<option selected>Choisissez un nombre</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</div>
						<div class="col-lg-1 mb-lg-3 mt-2">
							<button type="submit" class="btn text-wite shadow-none custom-bg">Envoyer</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Our Rooms -->
	<h2 class="mt-5 pt-4 mb-4 text-center fw-bold">NOS HÉBERGEMENTS</h2>
	<div class="container">
		<div class="row">
            <?php
            $room_res = select("SELECT * FROM `rooms` WHERE `status`=? AND `removed`=? ORDER BY `id` DESC LIMIT 3", [1,0], 'ii');
            while($room_data = mysqli_fetch_assoc($room_res)){
                // get features of room
                $fea_q = mysqli_query($con, "SELECT f.name FROM `features` f 
					INNER JOIN `room_features` rfea ON f.id = rfea.features_id 
					WHERE rfea.room_id = '$room_data[id]'");

                $features_data = "";
                while($fea_row = mysqli_fetch_assoc($fea_q)){
                    $features_data .="<span class='badge rounded-pill text-bg-light text-dark text-wrap me-1 mb-1'>$fea_row[name]</span>";
                }

                // get facility of room
                $fac_q = mysqli_query($con, "SELECT f.name FROM `facilities` f 
					INNER JOIN `room_facilities` rfac ON f.id = rfac.facilities_id 
					WHERE rfac.room_id = '$room_data[id]'");

                $facilities_data = "";
                while($fac_row = mysqli_fetch_assoc($fac_q)){
                    $facilities_data .="<span class='badge rounded-pill text-bg-light text-dark text-wrap me-1 mb-1'>$fac_row[name]</span>";
                }

                // get Thumbnail of image
                $room_thumb = ROOMS_IMG_PATH."thumbnail.jpg";
                $thumb_q = mysqli_query($con, "SELECT * FROM `room_images` 
					WHERE `room_id`='$room_data[id]' 
					AND `thumb`= '1'");

                if(mysqli_num_rows($thumb_q)>0){
                    $thumb_res = mysqli_fetch_assoc($thumb_q);
                    $room_thumb = ROOMS_IMG_PATH.$thumb_res['image'];
                }

                // print room card
                echo <<<data
					<div class="col-lg-4 col-md-6 my-3">
                        <div class="card border-0 shadow" style="max-width: 350px; margin: auto;">
                            <img src="$room_thumb" class="card-img-top">
                            <div class="card-body">
                                <h5>$room_data[name]</h5>
                                <h6 class="mb-4">$room_data[price]€</h6>
                                <div class="features mb-4">
                                    <h6 class="mb-1">Caractéristiques</h6>
                                    $features_data
                                </div>
                                <div class="facilities mb-4">
                                    <h6 class="mb-1">Équipements</h6>
                                    $facilities_data
                                </div>
                                <div class="guests mb-4">
                                    <h6 class="mb-1">Occupants</h6>
                                    <span class="badge rounded-pill bg-light text-dark text-wrap">$room_data[adult] Adulte(s)</span>
                                    <span class="badge rounded-pill bg-light text-dark text-wrap">$room_data[children] Enfant(s)</span>
								</div>
                                <div class="rating mb-4">
                                    <h6 class="mb-1">Notes</h6>
                                    <span class="badge rounded-pill bg-light">
                                        <i class="bi bi-star-fill text-warning"></i>
                                        <i class="bi bi-star-fill text-warning"></i>
                                        <i class="bi bi-star-fill text-warning"></i>
                                        <i class="bi bi-star-fill text-warning"></i>
                                    </span>
                                </div>
                                <div class="d-flex justify-content-evenly mb-2">
                                    <a href="#" class="btn btn-sm text-white custom-bg shadow-none">Reservez Maintenant</a>
                                    <a href="room_details.php?id=$room_data[id]" class="btn btn-sm btn-outline-dark shadow-none">Plus d'infos</a>
                                </div>
                            </div>
                        </div>
                    </div>
data;
            }

            ?>

			<div class="col-lg-12 text-center mt-5">
				<a href="hebergement.php" class="btn btn-sm btn-outline-dark rounded-0 fw-bold shadow-none">Plus d'Hébergements >>></a>
			</div>
		</div>
	</div>

	<!-- Facilities -->
	<h2 class="mt-5 pt-4 mb-4 text-center fw-bold">NOS INSTALLATIONS</h2>
	<div class="container">
		<div class="row justify-content-evenly px-lg-0 px-md-0 px-5">
            <?php
            $res = mysqli_query($con, "SELECT * FROM `facilities` ORDER BY `id` DESC LIMIT 5 ");
            $path = FACILITY_IMG_PATH;

            while($row = mysqli_fetch_assoc($res)){
                echo<<<data
					<div class="col-lg-2 col-md-2 text-center bg-white rounded shadow py-4 my-3">
                        <img src="$path$row[icon]" width="60px">
                        <h5 class="mt-3">$row[name]</h5>
                    </div>
data;
            }
            ?>
			<div class="col-lg-12 text-center mt-5">
			<a href="facilities.php" class="btn btn-sm btn-outline-dark rounded-0 fw-bold shadow-none">Toutes nos installations >>></a>
			</div>
		</div>
	</div>

	<!-- Testimonials -->
	<h2 class="mt-5 pt-4 mb-4 text-center fw-bold">LES AVIS</h2>
	<div class="container">
		<div class="swiper swiper-testimonial">
			<div class="swiper-wrapper mb-5">

				<div class="swiper-slide bg-white p-4">
					<div class="profile d-flex align-items-center p-4">
						<img src="./image/installations/wifi.svg" width="30px">
						<h6 class="m-0 ms-2">John Doe</h6>
					</div>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Est exercitationem ratione officia incidunt tempore? Illo suscipit odit libero veritatis necessitatibus.</p>
					<div class="rating">
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
					</div>
				</div>

				<div class="swiper-slide bg-white p-4">
					<div class="profile d-flex align-items-center p-4">
						<img src="./image/installations/wifi.svg" width="30px">
						<h6 class="m-0 ms-2">John Doe</h6>
					</div>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Est exercitationem ratione officia incidunt tempore? Illo suscipit odit libero veritatis necessitatibus.</p>
					<div class="rating">
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
					</div>
				</div>

				<div class="swiper-slide bg-white p-4">
					<div class="profile d-flex align-items-center p-4">
						<img src="./image/installations/wifi.svg" width="30px">
						<h6 class="m-0 ms-2">John Doe</h6>
					</div>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Est exercitationem ratione officia incidunt tempore? Illo suscipit odit libero veritatis necessitatibus.</p>
					<div class="rating">
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
					</div>
				</div>

				<div class="swiper-slide bg-white p-4">
					<div class="profile d-flex align-items-center p-4">
						<img src="./image/installations/wifi.svg" width="30px">
						<h6 class="m-0 ms-2">John Doe</h6>
					</div>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Est exercitationem ratione officia incidunt tempore? Illo suscipit odit libero veritatis necessitatibus.</p>
					<div class="rating">
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
						<i class="bi bi-star-fill text-warning"></i>
					</div>
				</div>

			</div>
			<div class="swiper-pagination"></div>
		</div>
        <div class="col-lg-12 text-center mt-5">
            <a href="about.php" class="btn btn-sm btn-outline-dark rounded-0 fw-bold shadow-none">Voir plus >>></a>
        </div>
	</div>

	<!-- Reach us -->
	

	<h2 class="mt-5 pt-4 mb-4 text-center fw-bold">NOUS TROUVER</h2>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 p-4 mb-lg-0 mb-3 bg-white rounded">
			<iframe height="320px" class="w-100 rounded" src="<?php echo $contact_r['iframe']; ?>" loading= "lazy"></iframe>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="bg-white p-4 rounded mb-4">
					<h5>Appeler Nous</h5>
					<a href="tel: <?php echo $contact_r['pn1']; ?>" class="d-inline-block mb-2 text-decoration-none text-dark"><i class="bi bi-telephone-fill"></i> <?php echo $contact_r['pn1']; ?></a><br>
					<?php 
						if($contact_r['pn2']!=''){
							echo<<<data
								<a href="tel: $contact_r[pn2]" class="d-inline-block text-decoration-none text-dark"><i class="bi bi-telephone-fill"></i> $contact_r[pn2]</a>
							data;
						}
					?>
					
				</div>
				<div class="bg-white p-4 rounded mb-4">
					<h5>Suivez Nous</h5>
					<?php
						if($contact_r['tw']!=""){
							echo<<<data
							<a href="$contact_r[tw]" class="d-inline-block mb-3"><span class="badge bg-light text-dark fs-6 p-2"><i class="bi bi-twitter me-1"></i> Twitter</span></a><br>
							data;
						}
					?>
					<a href="<?php echo $contact_r['fb']; ?>" class="d-inline-block mb-3"><span class="badge bg-light text-dark fs-6 p-2"><i class="bi bi-facebook me-1"></i> Facebook</span></a><br>
					<a href="<?php echo $contact_r['insta']; ?>" class="d-inline-block"><span class="badge bg-light text-dark fs-6 p-2"><i class="bi bi-instagram me-1"></i> Instagram</span></a><br>
				</div>
			</div>
		</div>
	</div>

	<?php  require('inc/footer.php'); ?>
<!-- JS -->
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script src="./js/main.js"></script>
</body>
</html>
