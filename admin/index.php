<?php 
    require("inc/essentials.php");
    require('inc/db_config.php'); 

    session_start();
    session_regenerate_id(true);
    if((isset($_SESSION['adminLogin']) && $_SESSION['adminLogin']==true)){
        redirect('dashboard.php');
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php require('inc/links.php'); ?>
    <title>Administration</title>
</head>
<body class="bg-light">
        <div class="login-form text-center rounded bg-white shadow overflow-hidden ">
            <form method="POST">
                <h4 class="bg-dark text-white py-3">PAGE D'ADMINISTRATION</h4>
                <div class="p-4">
                    <div class="mb-3">
                        <input name="admin_name" type="text" class="form-control shadow-none text-center" placeholder="Nom" required>
                    </div>
                    <div class="mb-4">
                        <input name="admin_pass" type="password" class="form-control shadow-none text-center" placeholder="Mot de passe" required>
					</div>
                    <button name="login" type="submit" class="btn text-white custom-bg shadow-none">Se connecter</button>
                </div>
            </form>
        </div>

        <?php 
        // On parcours les données récupérées lors de la validation du formulaire pour voir si elle correspondent à celle  qu'on à en BDD
        if(isset($_POST['login'])){
            $frm_data = filteration($_POST);
            
            $query = "SELECT * FROM `admin_cred` WHERE `admin_name`=? AND `admin_pass`=?";
            $values = [$frm_data['admin_name'], $frm_data['admin_pass']];

            $res = select($query, $values, "ss");
            if($res->num_rows==1){
                $row = mysqli_fetch_assoc($res);
                $_SESSION['adminLogin'] = true;
                $_SESSION['adminId'] = $row['sr_no'];
                redirect('dashboard.php');
            }
            else{
                alert('Erreur', 'Échec de connexion - Nom et(ou) Mot de passe invalide');
            }
        }
        
        ?>


    <?php require('inc/scripts.php'); ?>
</body>
</html>