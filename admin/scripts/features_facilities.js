
    let feature_s_form = document.getElementById('feature_s_form')
    let facility_s_form = document.getElementById('facility_s_form')

    feature_s_form.addEventListener('submit', function(e){
        e.preventDefault()
        add_feature()
    })

    function add_feature(){
        let data = new FormData()
        data.append('name', feature_s_form.elements['feature_name'].value)
        data.append('add_feature', '')

        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/feature_facilitie_crud.php", true)

        xhr.onload = function(){
            let myModal = document.getElementById('feature-s')
            let modal = bootstrap.Modal.getInstance(myModal) // Returns a Bootstrap modal instance
            modal.hide()

            if(this.responseText == 1){
                alert('success', 'Une nouvelle caractéristique à été ajoutée')
                feature_s_form.elements['feature_name'].value=''
                get_features()
            }
            else{
                alert('error', 'Aucun ajout. Le serveur ne répond pas.')
            }
        }
        xhr.send(data)
    }

    function get_features(){
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/feature_facilitie_crud.php", true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

        xhr.onload = function(){
            document.getElementById('features-data').innerHTML = this.responseText
        }
        xhr.send('get_features')
    }

    function rem_feature(val){
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/feature_facilitie_crud.php", true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

        xhr.onload = function(){
            if(this.responseText == 1){
                alert('success', 'La caractèristique à été supprimée.')
                get_features()
            }
            else if(this.resqponseText == 'room_added'){
                alert('error', 'Caractéristique ajouté à l\'hébergement.')
            }
            else{
                alert('error', 'Aucune modification.')
            }
        }
        xhr.send('rem_feature='+val)
    }

    facility_s_form.addEventListener('submit', function(e){
        e.preventDefault()
        add_facility()
    })

    function add_facility(){
        let data = new FormData()
        data.append('name', facility_s_form.elements['facility_name'].value)
        data.append('icon', facility_s_form.elements['facility_icon'].files[0])
        data.append('desc', facility_s_form.elements['facility_desc'].value)
        data.append('add_facility', '')

        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/feature_facilitie_crud.php", true)

        xhr.onload = function(){
            let myModal = document.getElementById('facility-s')
            let modal = bootstrap.Modal.getInstance(myModal) // Returns a Bootstrap modal instance
            modal.hide()

            if(this.responseText == 'inv_img'){
            alert('error', 'Seul les format SVG sont autorisés.')
            }
            else if(this.responseText == 'inv_size'){
                alert('error', 'L\'image doit faire moins de 1Mo.')
            }
            else if(this.responseText == 'upd_failed'){
                alert('error', 'Image non télécharger. Le serveur ne répond pas.')
            }
            else{
                alert('success', 'Une nouvelle installation à été ajoutée')
                facility_s_form.reset()
                get_facilities()
            }
        }
        xhr.send(data)
    }

    function get_facilities(){
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/feature_facilitie_crud.php", true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

        xhr.onload = function(){
            document.getElementById('facilities-data').innerHTML = this.responseText
        }
        xhr.send('get_facilities')
    }

    function rem_facility(val){
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/feature_facilitie_crud.php", true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

        xhr.onload = function(){
            if(this.responseText == 1){
                alert('success', 'L\'installation à été supprimée.')
                get_facilities()
            }
            else if(this.resqponseText == 'room_added'){
                alert('error', 'Installation ajouté à l\'hébergement.')
            }
            else{
                alert('error', 'Aucune modification.')
            }
        }
        xhr.send('rem_facility='+val)
    }

    window.onload = function(){
        get_features()
        get_facilities()
    }
