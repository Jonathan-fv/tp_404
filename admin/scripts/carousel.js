let carousel_s_form = document.getElementById('carousel_s_form')
let carousel_picture_inp = document.getElementById('carousel_picture_inp')

    carousel_s_form.addEventListener('submit', function(e){
        e.preventDefault()
        add_image()
    })

    function get_carousel(){
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/carousel_crud.php", true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

        xhr.onload = function(){
            document.getElementById('carousel-data').innerHTML = this.responseText
        }
        xhr.send('get_carousel')
    }

    function add_image(){
        let data = new FormData()
        data.append('picture', carousel_picture_inp.files[0])
        data.append('add_image', '')

        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/carousel_crud.php", true)

        xhr.onload = function(){
            let myModal = document.getElementById('carousel-s')
            let modal = bootstrap.Modal.getInstance(myModal) // Returns a Bootstrap modal instance
            modal.hide()

            if(this.responseText == 'inv_img'){
                alert('error', 'Seul les format JPEG, JPG, PNG et WEBP sont autorisés.')
            }
            else if(this.responseText == 'inv_size'){
                alert('error', 'L\'image doit faire moins de 2Mo.')
            }
            else if(this.responseText == 'upd_failed'){
                alert('error', 'Image non télécharger. Le serveur ne répond pas.')
            }
            else{
                alert('success', 'Une nouvelle photo à été ajouter')
                carousel_picture_inp.value=''
                get_carousel()
            }
        }

        xhr.send(data)
    }

    function rem_image(val){
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "ajax/carousel_crud.php", true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

        xhr.onload = function(){
            if(this.responseText == 1){
                alert('success', 'La photo à été supprimé.')
                get_carousel()
            }
            else{
                alert('error', 'Aucune modification.')
            }
        }
        xhr.send('rem_image='+val)
    }

    window.onload = function(){
        get_carousel()
    }

