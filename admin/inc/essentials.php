<?php

    //Frontend data
    define('SITE_URL','http://127.0.0.1/camping/');
    define('STAFF_IMG_PATH', SITE_URL.'image/staff/');
    define('CAROUSEL_IMG_PATH', SITE_URL.'image/carrousel/');
    define('FACILITY_IMG_PATH', SITE_URL.'image/installations/');
    define('ROOMS_IMG_PATH', SITE_URL.'image/hebergement/');


    //Backend data
    define('UPLOAD_IMAGE_PATH', $_SERVER['DOCUMENT_ROOT'].'/camping/image/');
    define('STAFF_FOLDER', 'staff/');
    define('CAROUSEL_FOLDER', 'carrousel/');
    define('FACILITY_FOLDER', 'installations/');
    define('ROOMS_FOLDER', 'hebergement/');


    function adminLogin(){
        session_start();
        if(!(isset($_SESSION['adminLogin']) && $_SESSION['adminLogin']==true)){
            echo "<script>
                window.location.href='index.php';
            </script>";
            exit;
        }
    }

    function redirect($url){
        echo "<script>
                window.location.href='$url';
            </script>";
            exit;
    }

    function alert($type, $msg){
        $bs_class = ($type == "succes") ? "alert-success" : "alert-danger" ;
        echo <<<alert
                <div class="alert $bs_class alert-dismissible fade show custom-alert" role="alert">
                    <strong class="me-3">$msg</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            alert;
    }

    function uploadImage($image, $folder){
        $valid_mime = ['image/jpeg', 'image/jpg', 'image/png', 'image/webp'];
        $img_mime = $image['type'];

        if(!in_array($img_mime,$valid_mime)){
            return 'inv_img'; // Format de l'image invalide
        }
        else if(($image['size']/(1024*1024))>2){
            return 'inv_size'; //Taille de l'image supérieur a 2Mb
        }
        else{
            $ext = pathinfo($image['name'], PATHINFO_EXTENSION);
            $rname = 'IMG_'.random_int(11111,99999).".$ext";

            $img_path = UPLOAD_IMAGE_PATH.$folder.$rname;
            if(move_uploaded_file($image['tmp_name'], $img_path)){
                return $rname;
            }
            else{
                return 'upd_failed';
            }

        }
    }

    function deleteImage($image, $folder){
        if(unlink(UPLOAD_IMAGE_PATH.$folder.$image)){
            return true;
        }
        else{
            return false;
        }
    }

    function uploadSVGImage($image, $folder){
        $valid_mime = ['image/svg+xml'];
        $img_mime = $image['type'];

        if(!in_array($img_mime,$valid_mime)){
            return 'inv_img'; // Format de l'image invalide
        }
        else if(($image['size']/(1024*1024))>1){
            return 'inv_size'; //Taille de l'image supérieur a 1Mb
        }
        else{
            $ext = pathinfo($image['name'], PATHINFO_EXTENSION);
            $rname = 'IMG_'.random_int(11111,99999).".$ext";

            $img_path = UPLOAD_IMAGE_PATH.$folder.$rname;
            if(move_uploaded_file($image['tmp_name'], $img_path)){
                return $rname;
            }
            else{
                return 'upd_failed';
            }

        }
    }

?>