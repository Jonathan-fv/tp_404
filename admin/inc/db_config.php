<?php 

    //On définit les valeurs pour la connexion à la BDD
    $hname = 'localhost';
    $uname = 'root';
    $pass = '';
    $db = 'camping';

    //On se connecte à la BDD
    $con = mysqli_connect($hname,$uname,$pass,$db);

    //On définit un message d'erreur si il y a un problème lors de la connexion à la BDD
    if(!$con){
        die("Connexion à la BDD impossible".mysqli_connect_error());
    }

    function filteration($data){
        //On sécurise ce que l'utilisateur nous envoie pour éviter un maximum de faille de sécurité
        foreach($data as $key => $value){
            $value = trim($value); //On supprime les espaces avant et après
            $value = stripslashes($value); //On supprime les antislash 
            $value = htmlspecialchars($value); //On ecrit les balises tel quel sans les interprétées
            $value = strip_tags($value); //On supprime les balises HTML et PHP qu'on nous envoie

            $data[$key] = $value;
        }
        return $data;
    }

    function selectAll($table){
        $con = $GLOBALS['con'];
        $res = mysqli_query($con, "SELECT * FROM $table");
        return $res;
    }

    function select($sql, $values, $datatypes){
        $con = $GLOBALS['con'];
        if($stmt = mysqli_prepare($con,$sql)){
            mysqli_stmt_bind_param($stmt,$datatypes, ...$values); // les ... sert à fournir une liste d'arguments de longueur variable aussi appeler SPLAT OPERATOR
            if(mysqli_stmt_execute($stmt)){
                $res = mysqli_stmt_get_result($stmt);
                mysqli_stmt_close($stmt );
                return $res; 
            }
            else{
                mysqli_stmt_close($stmt );
                die("Éxécution de la requête impossible - SELECT");
            }
        }
        else{
            die("Préparation de la requête impossible - SELECT");
        }
    }

    function update($sql, $values, $datatypes){
        $con = $GLOBALS['con'];
        if($stmt = mysqli_prepare($con,$sql)){
            mysqli_stmt_bind_param($stmt,$datatypes, ...$values); // les ... sert à fournir une liste d'arguments de longueur variable aussi appeler SPLAT OPERATOR
            if(mysqli_stmt_execute($stmt)){
                $res = mysqli_stmt_affected_rows($stmt);
                mysqli_stmt_close($stmt );
                return $res; 
            }
            else{
                mysqli_stmt_close($stmt );
                die("Éxécution de la requête impossible - UPDATE");
            }
        }
        else{
            die("Préparation de la requête impossible - UPDATE");
        }
    }

    function insert($sql, $values, $datatypes){
        $con = $GLOBALS['con'];
        if($stmt = mysqli_prepare($con,$sql)){
            mysqli_stmt_bind_param($stmt,$datatypes, ...$values); // les ... sert à fournir une liste d'arguments de longueur variable aussi appeler SPLAT OPERATOR
            if(mysqli_stmt_execute($stmt)){
                $res = mysqli_stmt_affected_rows($stmt);
                mysqli_stmt_close($stmt );
                return $res; 
            }
            else{
                mysqli_stmt_close($stmt );
                die("Éxécution de la requête impossible - INSERT");
            }
        }
        else{
            die("Préparation de la requête impossible - INSERT");
        }
    }

    function delete($sql, $values, $datatypes){
        $con = $GLOBALS['con'];
        if($stmt = mysqli_prepare($con,$sql)){
            mysqli_stmt_bind_param($stmt,$datatypes, ...$values); // les ... sert à fournir une liste d'arguments de longueur variable aussi appeler SPLAT OPERATOR
            if(mysqli_stmt_execute($stmt)){
                $res = mysqli_stmt_affected_rows($stmt);
                mysqli_stmt_close($stmt );
                return $res; 
            }
            else{
                mysqli_stmt_close($stmt );
                die("Éxécution de la requête impossible - DELETE");
            }
        }
        else{
            die("Préparation de la requête impossible - DELETE");
        }
    }
?>