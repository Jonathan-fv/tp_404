<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Camping de luxe - A propos</title>
    <!-- CSS -->
	<link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css"/>
	<?php require('inc/links.php'); ?>
</head>
<body class="bg-light">
	<?php require('inc/header.php'); ?>

	<div class="my-5 px-4">
		<h2 class="fw-bold text-center">NOUS CONNAITRE</h2>
		<div class="h-line bg-dark"></div>
		<p class="text-center mt-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi quam, in exercitationem dignissimos labore voluptates officiis libero architecto molestias molestiae.</p>
	</div>
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-lo-6 col-md-5 mb-4 order-lg-1 order-md-1 order-2">
				<h3 class="mb-3">Lorem ipsum dolor sit</h3>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus quia molestiae consequatur sit tenetur nobis culpa.Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus quia molestiae consequatur sit tenetur nobis culpa.</p>
			</div>
			<div class="col-lg-5 col-md-5 mb-4 order-lg-2 order-md-2 order-1">
				<img class="w-100" src="image/about/about.jpg" alt="photo du pdg">
			</div>
		</div>
	</div>
	<div class="container mt-5">
		<div class="row">
			<div class="col-lg-3 col-md-6 mb-4 px-4">
				<div class="bg-white rounded shadow p-4 border-top border-4 text-center box">
					<img src="image/about/tent-svgrepo-com.svg" alt="camping" width="70px">
					<h4 class="mt-3 info-about">100+ HÉBERGEMENTS</h4>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-4 px-4">
				<div class="bg-white rounded shadow p-4 border-top border-4 text-center box">
					<img src="image/about/customers-svgrepo-com.svg" alt="camping" width="70px">
					<h4 class="mt-3 info-about">3500+ CLIENTS</h4>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-4 px-4">
				<div class="bg-white rounded shadow p-4 border-top border-4 text-center box">
					<img src="image/about/customer-reviews-svgrepo-com.svg" alt="camping" width="70px">
					<h4 class="mt-3 info-about">2000+ COMMENTAIRES</h4>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-4 px-4">
				<div class="bg-white rounded shadow p-4 border-top border-4 text-center box">
					<img src="image/about/team-svgrepo-com.svg" alt="camping" width="70px">
					<h4 class="mt-3 info-about">200+ ÉQUIPIERS</h4>
				</div>
			</div>
		</div>
	</div>

	<h3 class="my-5 fw-bold h-font text-center">ÉQUIPE DE DIRECTION</h3>
	<div class="container px-4">
	<div class="swiper swiper-team">
		<div class="swiper-wrapper mb-5">
			<?php 
				$about_r = selectAll('team_details');
				$path = STAFF_IMG_PATH;
				while($row = mysqli_fetch_assoc($about_r)){
					echo<<<data
					<div class="swiper-slide bg-white text-center ocerflow-hidden rounded">
						<img src="$path$row[picture]" class="w-100">
						<h5 class="mt-2">$row[name]</h5>
					</div>
					data;
				}

			?>	
		</div>
		<div class="swiper-pagination"></div>
    </div>
	</div>

	<?php  require('inc/footer.php'); ?>
<!-- JS -->
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script src="./js/main.js"></script>
</body>
</html>
